<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDtlpembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dtlpembelian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_nota');
            $table->string('kode_barang');
            $table->float('qty');
            $table->float('harga');
            $table->float('total');

            $table->foreign('no_nota')->references('no_nota')->on('pembelian')->onDelete('cascade');
            $table->foreign('kode_barang')->references('kode_barang')->on('barang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtlpembelian');
    }
}
